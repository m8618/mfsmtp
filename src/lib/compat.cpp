// Copyright 2025 www.mumbleface.net  -- john wiggins
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
//    and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
//    or promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
// USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//   https://www.mumbleface.net: mfsmtp
//

#include <string>
#include <vector>
#include "compat.h"

namespace mfsmtp {

bool Connect(SOCKET sockfd, const SOCKADDR_IN& addr) {
#ifdef WIN32
   return bool(connect(sockfd, (sockaddr*)&addr, addr.get_size()) != SOCKET_ERROR);
#else
   return bool(connect(sockfd, (sockaddr*)&addr, addr.get_size()) == 0);
#endif
}

bool Socket(SOCKET& s, int domain, int type, int protocol) {
   s = socket(AF_INET, type, protocol);
#ifdef WIN32
   return bool(s != INVALID_SOCKET);
#else
   return bool(s != -1);
#endif   
}

bool Send(int &CharsSent, SOCKET s, const char *msg, size_t len, int flags) {
   CharsSent = send(s, msg, len, flags);
#ifdef WIN32
	return bool((CharsSent != SOCKET_ERROR));
#else
	return bool((CharsSent != -1));
#endif   
}

bool Recv(int &CharsRecv, SOCKET s, char *buf, size_t len, int flags) {
   CharsRecv = recv(s, buf, len, flags);
#ifdef WIN32
	return bool((CharsRecv != SOCKET_ERROR));
#else
	return bool((CharsRecv != -1));
#endif  
}

// just wrap the call to shutdown the connection on a socket
// this way I don't have to call it this way everywhere.
void Closesocket(const SOCKET& s) {
#ifdef WIN32
	closesocket(s);
#else
	close(s);
#endif
}

// This does nothing on unix.
// for windoze only, to initialise networking, snore
void initNetworking() {
//#ifdef SSLSTARTTLS
//   SSL_load_error_strings();
//#endif

#ifdef WIN32
	class socks
	{
	public:
		bool init() {

			WORD wVersionRequested;
			WSADATA wsaData;

			wVersionRequested = MAKEWORD( 2, 0 );
			int ret = WSAStartup( wVersionRequested, &wsaData);
			if(ret)
				return false;
			initialised = true;
			return true;
		}
		bool IsInitialised() const {return initialised;}
		socks():initialised(false){init();}
		~socks()
		{
			if(initialised)
				shutdown();
		}
	private:
		void shutdown(){WSACleanup();}
		bool initialised;
	};
	static socks s;
#endif
}

} // end namespace mfsmtp

