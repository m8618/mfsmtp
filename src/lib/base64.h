// Copyright 2025 www.mumbleface.net  -- john wiggins
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
//    and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
//    or promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
// USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//   https://www.mumbleface.net: mfsmtp
//
// base64.h: base64 encoding functions
//

#ifndef __BASE64_H__
#define __BASE64_H__

namespace mfsmtp {

// added the B64 to all members of the enum for SunOS (thanks Ken Weinert)
enum BASE64
{
	B64_A, B64_B, B64_C, B64_D, B64_E, B64_F, B64_G, B64_H, B64_I, B64_J, B64_K, B64_L, B64_M, B64_N, B64_O, B64_P, B64_Q, B64_R, B64_S, B64_T, B64_U, B64_V, B64_W, B64_X, B64_Y, B64_Z,
	B64_a, B64_b, B64_c, B64_d, B64_e, B64_f, B64_g, B64_h, B64_i, B64_j, B64_k, B64_l, B64_m, B64_n, B64_o, B64_p, B64_q, B64_r, B64_s, B64_t, B64_u, B64_v, B64_w, B64_x, B64_y, B64_z,
	B64_0, B64_1, B64_2, B64_3, B64_4, B64_5, B64_6, B64_7, B64_8, B64_9, plus, slash, padding
};

char getbase64character(const char& in);
std::vector<char> base64encode(const std::vector<char>& input, const bool returns = true);
std::string base64encode(const std::string& input, const bool returns = true);

} // end namespace mfsmtp


#endif //  #ifndef __BASE64_H__

