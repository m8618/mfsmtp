// Copyright 2025 www.mumbleface.net  -- john wiggins
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
//    and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
//    or promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
// USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//   https://www.mumbleface.net - mfsmtp
//
//
#include <iostream>
// Please note the mfsmtp library has to be installed
// for this next header to work.
#include "src/lib/mfsmtp.h"

// obviously we must escape the quotes i.e. \"
std::string html("<html>"
"<body>"
"This is the html part of the message<br><br>"
"<b>bold</b><br>"
"<i>italic</i><br>"
"<font size=\"7\">Large Text</font><br><br>"
"Or a link: <a href=\"https://mumbleface.net\">mumbleface.net</a><br><br><br>"
"</body>"
"</html>");

int main(int argc, char* argv[])
{
   // replace the users 'to' and 'from' here before compiling this demo
   mfsmtp::mailer m("root", "root", "subject line",
                    "This is the plain text part of the message", "localhost",
                    mfsmtp::mailer::SMTP_PORT, false);

   // send a html file (remember you still can send an html file as an attachment
   // but calling this function will allow mime compatible clients to actually
   // display the html if the client is set to show html messages.
   //    m.setmessageHTMLfile("/home/myname/thefile.html");
   
   // Build our html from a string. You can also send html as a vector.
   // i.e.
   //    std::vector<char> htmlvec;
   //    ....add html to the vector.
   //    m.setmessageHTML(htmlvec);
   m.setmessageHTML(html);

   //m.username("someone@somewhere.net");
   //m.password("secret");
   m.send(); // send the mail
   std ::cout << m.response() << "\n";
   return 0;
}
