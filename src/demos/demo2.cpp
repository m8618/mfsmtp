// Copyright 2025 www.mumbleface.net  -- john wiggins
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
//    and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
//    or promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
// USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//   https://www.mumbleface.net: mfsmtp
//
// http://www.boost.org
//#include <boost\thread\thread.hpp>

#include <iostream>
#include <cstring>
#include "src/lib/mfsmtp.h"

using std::cout;
using std::cin;
using std::string;

void Usage() {
   cout << "mfSMTP library demo program\n"
           "demo2 <email toaddress> <email fromaddress> <smtpserver>\n"
           "   e.g.\n"
           "      demo2 recipient@there.com me@server.com mail.server.com\n";
}

int main(int argc, char* argv[])
{
   if(argc != 4) {
      Usage();
      return 0;
   }

   cout << "mfSMTP library demo program\n\n";
   string to(argv[1]);
   string from(argv[2]);
   string smtpserver(argv[3]);

   if(to.length() < 2 || from.length() < 2 || smtpserver.length() < 2) {
      Usage();
      return 0;
   }

   char str[2048];
   cout << "Please enter the subject of the mail\n";
   cin.getline(str, 500);	
   string subject(str);
   strncpy(str, "", sizeof(""));

   cout << "Please enter the message body end with \".\" on a line by itself\n";
   string mailmessage;
   while(true) {
      cin.getline(str, 2048);
      if(!strcmp(str, "."))
         break;
		
      mailmessage += str;
      mailmessage += "\r\n";
      strncpy(str, "", sizeof(""));
   }

   cout << "\nPlease wait sending mail\n";
   // This is how to tell the mailer class that we are using a direct smtp server
   // preventing the code from doing an MX lookup on 'smtpserver' i.e. passing
   // false as the last parameter.
   mfsmtp::mailer mail(to.c_str(), from.c_str(), subject.c_str(), mailmessage.c_str(),
                       smtpserver.c_str(), mfsmtp::mailer::SMTP_PORT, false);

   // using a local file as opposed to a full path.
   mail.attach("attach.png");

   // add another recipient (carbon copy)
   //mail.addrecipient("someoneelse@somewhere.net", mailer::Cc);

   // set a new smtp server! This is the same as setting a nameserver.
   // this depends on the constructor call. i.e. in the constructor
   // If MXLookup is true this is a nameserver
   // If MXLookup is false this is an smtp server
   //mail.setserver("mail.somewherefun.com");
   // same again except using an IP address instead.
   //mail.setserver("192.168.0.1");

   // boost::thread thrd(mail);
   // thrd.join(); // optional
   // or:-

   // Use authentication
   //mail.username("testuser");
   //mail.password("secret");
   // LOGIN authentication by default
   // if you want plain as opposed to login authentication
   //mail.authtype(mfsmtp::mailer::PLAIN);

   mail.send();
     // or mail.operator()();
   cout << mail.response() << "\n";

   //mail.reset(); // now we can mail someone else.
   //mail.setmessage("flibbletooting");
   //mail.setsubject("another message same object");
   //mail.attach("/home/user1/image.gif");
   // or a win example
   //mail.attach("C:\\image.gif");
   //mail.addrecipient("someoneelseagain@foobar.net");

   //mail.operator ()();
   //cout << mail.response() << "\n";
   return 0;
}
