#!/bin/bash
set -x    # echo commands run

libtoolize
aclocal
autoconf
autoheader
automake --add-missing
